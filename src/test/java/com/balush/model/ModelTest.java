package com.balush.model;

import com.balush.view.ConsoleView;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ModelTest {

    @Test
    public void testPingPong() {
        Model model = new Model(new ConsoleView());
        assertEquals(20, model.startPingPong(4).length());
    }

    @Test
    public void testFibonacciSumExecutors() {
        Model model = new Model(new ConsoleView());
        List<Integer> listOfLastNumberFibonacci = Arrays.asList(1, 1, 2, 4, 7, 12, 20, 33, 54, 88);
        assertEquals(listOfLastNumberFibonacci, model.produceSumOfFibonacciUsingExecutors());
    }

    @Test
    public void testCriticalSection() {
        Model model = new Model(new ConsoleView());
        model.runCriticalSection();
        assertEquals(9000, CriticalClass.A);
    }
}
