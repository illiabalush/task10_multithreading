package com.balush.model;

import com.balush.model.lock.Lock;
import com.balush.model.lock.ReadWriteLock;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LockTest {

    @Test
    public void testUsualLock() {
        Lock lock = new Lock();
        int timeProgram = 6000;
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        long start = System.currentTimeMillis();
        executorService.submit(() -> {
           lock.lock();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        });
        executorService.submit(() -> {
            lock.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        });
        executorService.submit(() -> {
            lock.lock();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        });
        long liveTime = start + timeProgram;
        executorService.shutdown();
        assertTrue(checkTime(liveTime, System.currentTimeMillis()));
    }

    @Test
    public void testReadWriteLock() {
        ReadWriteLock readWriteLock = new ReadWriteLock();
        Lock readLock = readWriteLock.readLock();
        Lock writeLock = readWriteLock.writeLock();
        int timeProgram = 4000;
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        long start = System.currentTimeMillis();
        executorService.submit(() -> {
            readLock.lock();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readLock.unlock();
        });
        executorService.submit(() -> {
            readLock.lock();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readLock.unlock();
        });
        executorService.submit(() -> {
            writeLock.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            writeLock.unlock();
        });
        long liveTime = start + timeProgram;
        executorService.shutdown();
        assertTrue(checkTime(liveTime, System.currentTimeMillis()));
    }

    private boolean checkTime(long liveTime, long currentTime) {
        return liveTime / 10000 == currentTime / 10000;
    }
}
