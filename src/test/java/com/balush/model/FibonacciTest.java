package com.balush.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FibonacciTest {

    @Test
    public void testSequenceFibonacci() throws InterruptedException {
        Fibonacci fibonacci = new Fibonacci(6);
        fibonacci.start();
        fibonacci.join();
        assertEquals(6, fibonacci.getSequenceOfFibonacci().chars().filter(c -> c == ' ').count());
    }

    @Test
    public void testResultFibonacci() throws InterruptedException {
        Fibonacci fibonacci = new Fibonacci(5);
        fibonacci.run();
        fibonacci.join();
        assertEquals(12, fibonacci.getResult());
    }
}
