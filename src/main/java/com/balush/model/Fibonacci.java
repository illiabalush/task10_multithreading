package com.balush.model;

import java.util.concurrent.Callable;

class Fibonacci extends Thread implements Callable {
    private int numberOfSequence;
    private int sumAllNumbers;
    private StringBuffer sequenceOfFibonacci;

    public Fibonacci(int numberOfSequence) {
        super();
        this.numberOfSequence = numberOfSequence;
        sequenceOfFibonacci = new StringBuffer();
    }

    private int getFibonacciResult(int sumAllNumbers, int left, int right) {
        sequenceOfFibonacci.append(right).append(" ");
        this.sumAllNumbers += right;
        numberOfSequence--;
        return (numberOfSequence <= 0) ? right : getFibonacciResult(sumAllNumbers, right, right + left);
    }

    public int getNumberOfSequence() {
        return numberOfSequence;
    }

    public void setNumberOfSequence(int numberOfSequence) {
        this.numberOfSequence = numberOfSequence;
    }

    public int getResult() {
        return sumAllNumbers;
    }

    public String getSequenceOfFibonacci() {
        return sequenceOfFibonacci.toString();
    }

    @Override
    public void run() {
        getFibonacciResult(sumAllNumbers, 0, 1);
    }

    @Override
    public Integer call() {
        getFibonacciResult(sumAllNumbers, 0, 1);
        return sumAllNumbers;
    }
}
