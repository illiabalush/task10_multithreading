package com.balush.model.lock;

public class ReadWriteLock {
    protected boolean isLocked = false;
    protected Thread ownerThread;
    protected TypeThread typeOwnerThread;
    protected int lockedCount = 0;

    protected enum TypeThread {
        Writer, Reader
    }

    public Lock readLock() {
        return new Lock(TypeThread.Reader);
    }

    public Lock writeLock() {
        return new Lock(TypeThread.Writer);
    }
}
