package com.balush.model.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

public class Lock extends ReadWriteLock implements java.util.concurrent.locks.Lock {
    private TypeThread typeThread;

    public Lock() {
        typeThread = TypeThread.Writer;
    }

    public Lock(TypeThread typeThread) {
        this.typeThread = typeThread;
    }

    public synchronized void lock() {
        Thread callingThread = Thread.currentThread();
        // the second condition wrote for reEntrance
        while (isLocked && ownerThread != callingThread && typeOwnerThread == TypeThread.Writer) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        isLocked = true;
        lockedCount++;
        typeOwnerThread = typeThread;
        ownerThread = callingThread;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    public synchronized void unlock() {
        if (Thread.currentThread() == ownerThread) {
            lockedCount--;
            if (lockedCount == 0) {
                typeOwnerThread = null;
                isLocked = false;
                notify();
            }
        }
    }

    @Override
    public Condition newCondition() {
        return null;
    }

    public void setTypeThread(TypeThread typeThread) {
        this.typeThread = typeThread;
    }
}
