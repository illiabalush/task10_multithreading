package com.balush.model;

import com.balush.model.lock.Lock;
import com.balush.view.View;

public class CriticalClass implements Runnable {
    static int A = 0;
    private View view;
    private final Lock lock;

    public CriticalClass(View view, Lock lock) {
        this.view = view;
        this.lock = lock;
    }

    @Override
    public void run() {
        incrementFirst();
        incrementSecond();
        incrementThird();
    }

    private void incrementFirst() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                A++;
            }
            view.showMessage(String.valueOf(A));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } finally {
            lock.unlock();
        }
    }

    private void incrementSecond() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                A++;
            }
            view.showMessage(String.valueOf(A));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } finally {
            lock.unlock();
        }
    }

    private void incrementThird() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                A++;
            }
            view.showMessage(String.valueOf(A));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } finally {
            lock.unlock();
        }
    }
}
