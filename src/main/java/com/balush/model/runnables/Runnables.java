package com.balush.model.runnables;

import com.balush.view.View;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class Runnables {
    private View view;
    private static Object sync = new Object();

    public Runnables(View view) {
        this.view = view;
    }

    public Runnable runnablePing(int numberOfPingPong, StringBuffer stringBuffer) {
        return () -> {
            synchronized (sync) {
                for (int i = 0; i < numberOfPingPong / 2; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    stringBuffer.append("\nping");
                    sync.notify();
                }
            }
        };
    }

    public Runnable runnablePong(int numberOfPingPong, StringBuffer stringBuffer) {
        return () -> {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (sync) {
                for (int i = 0; i < numberOfPingPong / 2; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    stringBuffer.append("\npong");
                }
            }
        };
    }

    public Runnable getThrowRunnable(Scanner scanner, PipedOutputStream pipedOutputStream) {
        return () -> {
            for (int i = 0; i < 5; i++) {
                view.showMessage("Enter word[" + i + "] that will be thrown: ");
                try {
                    pipedOutputStream.write(scanner.nextLine().getBytes());
                    Thread.sleep(1000);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public Runnable getCatchRunnable(PipedInputStream pipedInputStream) {
        return () -> {
            for (int i = 0; i < 5; i++) {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    int j = pipedInputStream.read();
                    int isAvailable = pipedInputStream.available();
                    while (isAvailable != 0) {
                        stringBuilder.append((char) j);
                        j = pipedInputStream.read();
                        isAvailable = pipedInputStream.available();
                    }
                    stringBuilder.append((char) j);
                    view.showMessage("Catch: " + stringBuilder.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public Runnable getRunnableShowTime(long liveTime) {
        return () -> {
            while (System.currentTimeMillis() < liveTime) {
                view.showMessage((liveTime - System.currentTimeMillis()) / 1000 + " seconds remaining");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
            }
        };
    }

    public Runnable getRunnableProduceQueue(BlockingQueue<String> blockingQueue, long liveTime, Scanner scanner) {
        return () -> {
            while (System.currentTimeMillis() < liveTime) {
                view.showMessage("Enter word that will be thrown: ");
                try {
                    String stringToAnotherThread = scanner.nextLine();
                    blockingQueue.put(stringToAnotherThread);
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    view.showMessage("Time went out");
                }
            }
        };
    }

    public Runnable getRunnableConsumerQueue(BlockingQueue<String> blockingQueue, long liveTime) {
        return () -> {
            while (System.currentTimeMillis() < liveTime) {
                try {
                    String stringFromAnotherThread = blockingQueue.take();
                    view.showMessage("Catch: " + stringFromAnotherThread);
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    view.showMessage("Time went out");
                }
            }
        };
    }
}
