package com.balush.model;

import com.balush.model.lock.Lock;
import com.balush.model.runnables.Runnables;
import com.balush.view.View;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Model {
    private View view;
    private Runnables runnables;

    public Model(View view) {
        this.view = view;
        runnables = new Runnables(view);
    }

    public String startPingPong(int numberOfPingPong) {
        StringBuffer stringBuffer = new StringBuffer();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(runnables.runnablePing(numberOfPingPong, stringBuffer));
        executorService.submit(runnables.runnablePong(numberOfPingPong, stringBuffer));
        view.showMessage("Wait 3 seconds");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        return stringBuffer.toString();
    }

    public void produceSequenceOfFibonacci() {
        for (int i = 0; i < 10; i++) {
            Fibonacci fibonacci = new Fibonacci(i);
            fibonacci.start();
            try {
                fibonacci.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            view.showMessage(fibonacci.getSequenceOfFibonacci());
        }
    }

    public void produceSequenceOfFibonacciUsingExecutors() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 10; i++) {
            Fibonacci fibonacci = new Fibonacci(i);
            executorService.submit(() -> {
                fibonacci.run();
                view.showMessage(fibonacci.getSequenceOfFibonacci());
            });
        }
        executorService.shutdown();
    }

    public void produceSequenceOfFibonacciUsingScheduledExecutors() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        AtomicInteger count = new AtomicInteger(1);
        Runnable task = () -> {
            Fibonacci fibonacci = new Fibonacci(count.get());
            fibonacci.run();
            view.showMessage(fibonacci.getSequenceOfFibonacci());
            count.getAndIncrement();
        };
        executorService.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    public void createTasksAndRunUsingScheduledExecutors(int quantityOfTasks) {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        AtomicInteger atomicQuantityOfTasks = new AtomicInteger(quantityOfTasks);
        Runnable task = () -> {
            try {
                int randomSleepTime = new Random().nextInt(10000);
                Thread.sleep(randomSleepTime);
                view.showMessage("Sleep time: " + randomSleepTime);
                atomicQuantityOfTasks.decrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        executorService.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
        while (atomicQuantityOfTasks.get() > 0) ;
        executorService.shutdown();
    }

    public List<Integer> produceSumOfFibonacciUsingExecutors() {
        ExecutorService executorService = Executors.newWorkStealingPool();
        List<Callable<Integer>> callables = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            callables.add(new Fibonacci(i)::call);
        }
        List<Integer> listOfSumFibonacci = new ArrayList<>();
        try {
            listOfSumFibonacci = executorService.invokeAll(callables)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    })
                    .collect(Collectors.toList());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        return listOfSumFibonacci;
    }

    public void runCriticalSection() {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Lock lock = new Lock();
        executorService.submit(new CriticalClass(view, lock));
        executorService.submit(new CriticalClass(view, lock));
        executorService.submit(new CriticalClass(view, lock));
        try {
            Thread.sleep(9000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    public void usePipeToCommunicate() {
        Scanner scanner = new Scanner(System.in);
        PipedInputStream pipedInputStream = new PipedInputStream();
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        try {
            pipedInputStream.connect(pipedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread throwInfo = new Thread(runnables.getThrowRunnable(scanner, pipedOutputStream));
        Thread catchInfo = new Thread(runnables.getCatchRunnable(pipedInputStream));
        throwInfo.start();
        catchInfo.start();
        try {
            throwInfo.join();
            catchInfo.join();
            pipedInputStream.close();
            pipedOutputStream.close();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public void useBlockingQueueToCommunicate() {
        int timeProgram = 10000;
        LinkedBlockingDeque<String> blockingQueue = new LinkedBlockingDeque<>();
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        long start = System.currentTimeMillis();
        long liveTime = start + timeProgram;
        executorService.submit(runnables.getRunnableShowTime(liveTime));
        executorService.submit(runnables.getRunnableProduceQueue(blockingQueue, liveTime, new Scanner(System.in)));
        executorService.submit(runnables.getRunnableConsumerQueue(blockingQueue, liveTime));
        try {
            Thread.sleep(timeProgram);
            executorService.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdownNow();
        }
    }
}
