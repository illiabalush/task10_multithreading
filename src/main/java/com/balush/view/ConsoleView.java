package com.balush.view;

import com.balush.controller.Controller;
import com.balush.interfaces.Printable;

import java.util.*;

public class ConsoleView extends View {
    private Controller controller;
    private Scanner scanner;
    private Map<String, String> menu;
    private Map<String, Printable> menuItems;

    public ConsoleView() {
        controller = new Controller(this);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - ping pong");
        menu.put("2", "2 - produce sequence of Fibonacci numbers");
        menu.put("3", "3 - produce sequence of Fibonacci numbers using executor service");
        menu.put("4", "4 - create callable class that sums the values of all the Fibonacci numbers");
        menu.put("5", "5 - create tasks and run using ScheduledExecutors");
        menu.put("6", "6 - create class with critical sections and synchronized him");
        menu.put("7", "7 - create two tasks that use a pipe to communicate");
        menu.put("8", "8 - create two tasks that use blocking queue to communicate");
        menu.put("Q", "Q - exit");
        menuItems = new LinkedHashMap<>();
        menuItems.put("1", this::pingPong);
        menuItems.put("2", this::produceFibonacciSequence);
        menuItems.put("3", this::produceFibonacciSequenceUsingExecutor);
        menuItems.put("4", this::sumAllFibonacciValues);
        menuItems.put("5", this::createTaskUsingScheduledExecutors);
        menuItems.put("6", this::createClassWithCriticalSection);
        menuItems.put("7", this::connectTasksWithPipe);
        menuItems.put("8", this::connectTasksWithBlockingQueue);
    }

    private void pingPong() {
        logger.info("Enter number of ping pong: ");
        int numOfPingPong = scanner.nextInt();
        logger.info(controller.getModel().startPingPong(numOfPingPong));
    }

    private void produceFibonacciSequence() {
        controller.getModel().produceSequenceOfFibonacci();
    }

    private void produceFibonacciSequenceUsingExecutor() {
        controller.getModel().produceSequenceOfFibonacciUsingExecutors();
    }

    private void sumAllFibonacciValues() {
        logger.info(controller.getModel().produceSumOfFibonacciUsingExecutors());
    }

    private void createTaskUsingScheduledExecutors() {
        logger.info("Enter number of tasks: ");
        int numOfTasks = scanner.nextInt();
        controller.getModel().createTasksAndRunUsingScheduledExecutors(numOfTasks);
    }

    private void createClassWithCriticalSection() {
        controller.getModel().runCriticalSection();
    }

    private void connectTasksWithPipe() {
        controller.getModel().usePipeToCommunicate();
    }

    private void connectTasksWithBlockingQueue() {
        controller.getModel().useBlockingQueueToCommunicate();
    }

    private void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }

    @Override
    public void showMessage(String message) {
        logger.info(message);
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuItems.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }
}
