package com.balush.controller;

import com.balush.model.Model;
import com.balush.view.View;

public class Controller {
    private Model model;

    public Controller(View view) {
        model = new Model(view);
    }

    public Model getModel() {
        return model;
    }
}
